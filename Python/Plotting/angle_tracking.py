import numpy as np
import matplotlib.pyplot as plt
import csv
import math


steps = []
angle1 = []
angle2 = []
angle3 = []


def unit_vector(vector):
  return vector / np.linalg.norm(vector)


def angle_between(v1, v2):
  v1_u = unit_vector(v1)
  v2_u = unit_vector(v2)
  return math.degrees(np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0)))

def read():
  i = 0
  with open("positionsFile.txt", 'r') as file:
    csvreader = csv.reader(file)

    v1 = []
    v2 = []
    v3 = []
    v3 = []

    u1 = []
    u2 = []
    u3 = []

    for row in csvreader:
      steps.append(i)
      v1.append( float(row[0].replace('(','').replace(')','').replace(' ','') ))
      v1.append( float(row[1].replace('(', '').replace(')', '').replace(' ', '')))
      v1.append( float(row[2].replace('(', '').replace(')', '').replace(' ', '')))

      v2.append( float(row[3].replace('(', '').replace(')', '').replace(' ', '')))
      v2.append( float(row[4].replace('(', '').replace(')', '').replace(' ', '')))
      v2.append( float(row[5].replace('(', '').replace(')', '').replace(' ', '')))

      v3.append( float(row[6].replace('(', '').replace(')', '').replace(' ', '')))
      v3.append( float(row[7].replace('(', '').replace(')', '').replace(' ', '')))
      v3.append( float(row[8].replace('(', '').replace(')', '').replace(' ', '')))

      u1.append( float(row[9].replace('(','').replace(')','').replace(' ','') ))
      u1.append( float(row[10].replace('(', '').replace(')', '').replace(' ', '')))
      u1.append( float(row[11].replace('(', '').replace(')', '').replace(' ', '')))

      u2.append( float(row[12].replace('(', '').replace(')', '').replace(' ', '')))
      u2.append( float(row[13].replace('(', '').replace(')', '').replace(' ', '')))
      u2.append( float(row[14].replace('(', '').replace(')', '').replace(' ', '')))

      u3.append( float(row[15].replace('(', '').replace(')', '').replace(' ', '')))
      u3.append( float(row[16].replace('(', '').replace(')', '').replace(' ', '')))
      u3.append( float(row[17].replace('(', '').replace(')', '').replace(' ', '')))

      angle1.append(angle_between(v1,u1))
      print(np.dot(v1,u1))
      angle2.append(angle_between(v2, u2))
      angle3.append(angle_between(v3, u3))

      print(v1)
      print(v2)
      v1.clear()
      v2.clear()
      v3.clear()
      u1.clear()
      u2.clear()
      u3.clear()
      i += 1


def main():
  read()
  plt.plot(steps, angle1, label='Shoulder')
  plt.plot(steps, angle2, label='Elbow')
  plt.plot(steps, angle3,  label='Wrist')
  plt.legend()
  plt.ylabel('Degrees')
  plt.xlabel('Step')
  plt.title('Angle between tracked and received Positions')
  plt.show()


plt.rcParams.update({'font.size': 22})
main()