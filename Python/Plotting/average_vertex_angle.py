import numpy as np
import matplotlib.pyplot as plt
import csv
import math
import random

from tempfile import mkstemp
from shutil import move, copymode
from os import fdopen, remove

steps = []
angle = []

def unit_vector(vector):
  return vector / np.linalg.norm(vector)


def angle_between(v1, v2):
  v1_u = unit_vector(v1)
  v2_u = unit_vector(v2)
  return math.degrees(np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0)))

def read():
  i = 0
  with open("angles.txt", 'r') as file:
    Lines = file.readlines()


    for line in Lines:
      print(line)
      angle.append(float(line))
      steps.append(i)
      i += 1



def main():
  read()
  plt.plot(steps, angle, color='darkorange')
  plt.ylabel('Degrees')
  plt.xlabel('Steps')
  plt.title('Angle between average vertices')
  plt.show()


plt.rcParams.update({'font.size': 22})
main()