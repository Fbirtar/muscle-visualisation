using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation.Samples;
using UnityEngine.XR.ARFoundation;
using Unity.Collections;
using UnityEngine.UI;
using System.IO;

#if UNITY_IOS && !UNITY_EDITOR
using Unity.iOS.Multipeer;
using UnityEngine.XR.ARKit;
#endif

/// <summary>
/// Class responsible for the communication, as well as for controlling the components necessary for the simulation.
/// It is based on the persival GlobalCtrl.cs script.
/// </summary>
public class GlobalCtrl : MonoBehaviour
{
    /// <summary>
    /// Updates the joint positions when a human body event is triggered
    /// </summary>
    public HumanBodyTrackerPersival M_HumanBodyTracker;
    /// <summary>
    /// The material for rendering the biceps
    /// </summary>
    public Material Mat_Biceps;
    /// <summary>
    /// Controls the muscle activation
    /// </summary>
    private static MuscleActiCtrl m_muscleActiCtrl;
    /// <summary>
    /// Reference to the global control instance
    /// </summary>
    private static GlobalCtrl m_instance;
    /// <summary>
    /// The manager of all user interface
    /// </summary>
    private static UIManager m_uiManager;
    /// <summary>
    /// Controls the deformation model
    /// </summary>
    private static MeshCtrl m_meshCtrl;
    /// <summary>
    /// [obsolete] Backup solution, visualizing the deformation using point cloud (particle system)
    /// but just leave it there
    /// </summary>
    private static ParticalVisualizer m_partiVisualizer;
    /// <summary>
    /// The rendering center for using surface
    /// </summary>
    private static FaceVisualizer m_faceVisualizer;
    /// <summary>
    /// Calculates the angle score and maintains the angle score list
    /// </summary>
    private AngleScoreCalculator angleScoreCalculator;
    /// <summary>
    /// The name for this network service.
    /// See <a href="https://developer.apple.com/documentation/multipeerconnectivity/mcnearbyserviceadvertiser">MCNearbyServiceAdvertiser</a>
    /// for the purpose of and restrictions on this name.
    /// </summary>
    public string serviceType
    {
        get => m_ServiceType;
        set => m_ServiceType = value;
    }
    [SerializeField]
    [Tooltip("The name for this network service. It should be 15 characters or less and can contain ASCII, lowercase letters, numbers, and hyphens.")]
    string m_ServiceType;

    #region tracking data
    /// <summary>
    /// The direct dynamic positions of joints in world coordinates.
    /// </summary>
    [HideInInspector]
    public Vector3 LShoulder;
    [HideInInspector]
    public Vector3 LElbow;
    [HideInInspector]
    public Vector3 LWrist;
    [HideInInspector]
    public Vector3 RShoulder;
    [HideInInspector]
    public Vector3 RElbow;
    [HideInInspector]
    public Vector3 RWrist;
    public Vector3 Chest { get => (LShoulder + RShoulder) / 2; }
    public Vector3 BodyForward { get => Vector3.Cross(LShoulder - RShoulder, Vector3.up); }
    public Vector3 BodyLeft { get => LShoulder - RShoulder; }
    /// <summary>
    /// Vector from the left shoulder to the left elbow
    /// </summary>
    public Vector3 LS2E { get => LElbow - LShoulder; }
    /// <summary>
    /// Vector from the left elbow to the left wrist
    /// </summary>
    public Vector3 LE2W { get => LWrist - LElbow; }
    #endregion

    #region body angles
    /// <summary>
    /// The angle between upper arm and torso, its subscale on sagittal body plane.
    /// Similar for the others.
    /// </summary>
    public float Sagittal { get => Vector3.Angle(Vector3.ProjectOnPlane(LS2E, BodyLeft), Vector3.down); }
    public float Frontal { get => Vector3.Angle(Vector3.ProjectOnPlane(LS2E, BodyForward), Vector3.down); }
    public float Transverse { get => Vector3.Angle(Vector3.ProjectOnPlane(LS2E, Vector3.up), BodyForward); }
    #endregion

   
    /// <summary>
    /// List of angles between average vertices
    /// </summary>
    List<float> angles;
    /// <summary>
    /// Coordinates of the 30 vertices, stored as float array
    /// </summary>
    float[] actiCoords;
    /// <summary>
    /// The number of devices in the session
    /// </summary>
    int connectedPeerCount;
    /// <summary>
    /// Keeps track of the frames
    /// </summary>
    int step;
    /// <summary>
    /// Frame offset between peers and main device
    /// </summary>
    int stepDelta;
    /// <summary>
    /// Whether first time reset message was sent
    /// </summary>
    bool sentInitialization;
    /// <summary>
    /// Whether vertex coordinates were received in a frame
    /// </summary>
    bool receivedActiCoords;
    /// <summary>
    /// Keeps track of the number of vertex coordinate messages received
    /// </summary>
    int vertexDataCount;
    /// <summary>
    /// Text assets used to display information to the user
    /// </summary>
    public Text receivedVerticesText;
    public Text angleScoreText;
    public Text orderText;
    /// <summary>
    /// Path where evaluation data is saved
    /// </summary>
    private string filePath => Path.Combine(Application.persistentDataPath, "angles.txt");

    public static GlobalCtrl M_Instance
    {
        get
        {
            if (m_instance == null)
                m_instance = FindObjectOfType<GlobalCtrl>();
            return m_instance;
        }
    }
    public static ParticalVisualizer M_PartiVisualizer
    {
        get
        {
            if (m_partiVisualizer == null)
                m_partiVisualizer = FindObjectOfType<ParticalVisualizer>();
            return m_partiVisualizer;
        }
    }
    public static MeshCtrl M_MeshCtrl
    {
        get
        {
            if (m_meshCtrl == null)
                m_meshCtrl = FindObjectOfType<MeshCtrl>();
            return m_meshCtrl;
        }
    }
    public static MuscleActiCtrl M_MuscleActiCtrl
    {
        get
        {
            if (m_muscleActiCtrl == null)
                m_muscleActiCtrl = FindObjectOfType<MuscleActiCtrl>();
            return m_muscleActiCtrl;
        }
    }
    public static UIManager M_UIManager
    {
        get
        {
            if (m_uiManager == null)
                m_uiManager = FindObjectOfType<UIManager>();
            return m_uiManager;
        }
    }

    public static FaceVisualizer M_FaceVisualizer
    {
        get
        {
            if (m_faceVisualizer == null)
                m_faceVisualizer = FindObjectOfType<FaceVisualizer>();
            return m_faceVisualizer;
        }
    }

    void DisableNotSupported(string reason)
    {
        enabled = false;
    }

    void Start()
    {
        connectedPeerCount = 0;
        step = 0;
        stepDelta = 0;
        sentInitialization = false;
        vertexDataCount = 0;
        angles = new();
    }

    void OnEnable()
    {
        angleScoreCalculator = new();
        // Multipeer is only supported on iOS
#if UNITY_IOS && !UNITY_EDITOR
        m_MCSession.Enabled = true;
#else
        DisableNotSupported("This platform does not support Multipeer Connectivity.");
#endif
    }

    // Multipeer is only supported on iOS
#if UNITY_IOS && !UNITY_EDITOR 
    /// <summary>
    /// The multipeer session
    /// </summary>
    MCSession m_MCSession;


    void Awake()
    {
        m_MCSession = new MCSession(SystemInfo.deviceName, m_ServiceType);
        M_UIManager.f_Init();
    }

    void FixedUpdate()
    {
        step += 1;
        receivedActiCoords = false;
        // Displaying information
        receivedVerticesText.text = vertexDataCount.ToString();
        orderText.text = angleScoreCalculator.angleScoreOrder.ToString();
        // Angle score is updated every 30 frames
        if(step % 30 == 0)
        {
            angleScoreCalculator.UpdateAngleScore(M_HumanBodyTracker.boneController.skeletonRoot);
            angleScoreText.text = angleScoreCalculator.angleScore.ToString();
        }
        // The first 1000 steps are recorded
        if(vertexDataCount == 1000)
        {
           // WriteCsv();
        }
        // Perform reset if network structure changes
        if (connectedPeerCount != m_MCSession.ConnectedPeerCount)
        {
            Debug.Log("PeerCount has changed");
            connectedPeerCount = m_MCSession.ConnectedPeerCount;
            Reset();
        }

        if(m_MCSession.ConnectedPeerCount != 0)   // If other devices are connected
        {
            // Only send initial reset message once
            if(!sentInitialization)
            {
                sentInitialization = true;
                SendResetMessage();
                Reset();
            }
            // Check if new angle score was received and determine angle score order again
            CheckIncomingData();
            angleScoreCalculator.DetermineAngleScoreOrder();

            if(angleScoreCalculator.angleScoreOrder == 0) // This code block is only performed by the main device
            {
                // Send synchronization message every 300 frames
                if(step % 300 == 0)
                {
                    SendSyncMessage();
                }
                // Send reset message and perform reset every 30 frames
                if(step % 30 == 0)
                {
                    SendResetMessage();
                    Reset();
                }
                // Calculate vertex coordinates, send them to the peers, then update the arm model
                M_MuscleActiCtrl.UpdateActivation();
                actiCoords = M_MeshCtrl.CalculateOutputCoords();
                SendActiCoords();
                M_MeshCtrl.UpdateMeshDistributed(actiCoords);
                M_FaceVisualizer.UpdateVisualizer();
            }
            else // This code block is performed by the peers
            {
                // Check for vertex coordinates
                CheckIncomingData();
                if(!receivedActiCoords) // If no coordinates received by this point, calculate them locally
                {
                    M_MuscleActiCtrl.UpdateActivation();
                    actiCoords = M_MeshCtrl.CalculateOutputCoords();
                }
                // Update the arm model
                M_MeshCtrl.UpdateMeshDistributed(actiCoords);
                M_FaceVisualizer.UpdateVisualizer();
            }
        }
        else // If no other devices are connected
        {
            M_MuscleActiCtrl.UpdateActivation();
            M_MeshCtrl.UpdateMesh();
            M_FaceVisualizer.UpdateVisualizer();
        }
    }
    /// <summary>
    /// Calls for the reset of the angle score calculator, which is described in the AngleScoreCalculator class.
    /// </summary>
    void Reset()
    {
        Debug.Log("Score Reset");
        angleScoreCalculator.ResetAngleScores(M_HumanBodyTracker.boneController.skeletonRoot);
        SendScore();
    }
    /// <summary>
    /// Sends a message that triggers a reset
    /// </summary>
    void SendResetMessage()
    {
        Message message = new Message();
        message.Type = MessageType.Reset;
        SendMessage(message);
    }
    /// <summary>
    /// Sends a message that triggers an update of the frame offset
    /// </summary>
    void SendSyncMessage()
    {
        Message message = new Message();
        message.Type = MessageType.Sync;
        message.Step = step;
        SendMessage(message);
    }
    /// <summary>
    /// Sends a message containing the angle score
    /// </summary>
    void SendScore()
    {
        Debug.Log("Sent angle score");
        Message message = new Message();
        message.Type = MessageType.Score;
        message.Score = angleScoreCalculator.angleScore;
        SendMessage(message);
    }
    /// <summary>
    /// Sends a message containing vertex coordinates
    /// </summary>
    void SendActiCoords()
    {
        Message message = new Message();
        message.Type = MessageType.Vertices;
        message.Vertices = actiCoords;
        message.Step = step;
        SendMessage(message);
    }
    /// <summary>
    /// Converts a message object to json, then to NSData which is sent to the devices in the multipeer session
    /// </summary>
    void SendMessage(Message message)
    {
        string jsonString = JsonUtility.ToJson(message);
        NativeArray<byte> bytes = new NativeArray<byte>(System.Text.Encoding.UTF8.GetBytes(jsonString), Allocator.Temp);
        NativeSlice<byte> bytesSlice = new NativeSlice<byte>(bytes);
        using (var data = NSData.CreateWithBytesNoCopy(bytesSlice))
        {
            m_MCSession.SendToAllPeers(data, MCSessionSendDataMode.Reliable);
        }
    }
    /// <summary>
    /// Checks the incoming data for messages and performs a specific action for each message type
    /// </summary>
    void CheckIncomingData()
    {
        while (m_MCSession.ReceivedDataQueueSize > 0)
        {
            using (var data = m_MCSession.DequeueReceivedData())
            {
                // Recreate message object from received NSData
                var slice = data.Bytes;
                var bytes = new byte[slice.Length];
                slice.CopyTo(bytes);
                string receivedMessageJson = System.Text.Encoding.UTF8.GetString(bytes);
                Message receivedMessage = JsonUtility.FromJson<Message>(receivedMessageJson);

                if(receivedMessage.Type == MessageType.Vertices) 
                {
                    vertexDataCount += 1;
                    receivedActiCoords = true;
                    // Calculates the delay
                    int undelayedStep = step + stepDelta;
                    int delay = undelayedStep - receivedMessage.Step;

                    // Local vertices are calculated for the evaluation
                    M_MuscleActiCtrl.UpdateActivation();
                    float[] localVertices = M_MeshCtrl.CalculateOutputCoords();
                    float[] receivedVertices = receivedMessage.Vertices;

                    if(delay > 30) // If the message is too old, the local vertices are used
                    {  
                        actiCoords = localVertices;
                    }
                    else // Otherwise the received vertices are used
                    {
                        actiCoords = receivedVertices;
                    }

                    // Stores angle between first 1000 local and received average vertices for the evaluation
                    //if(vertexDataCount == 1000)
                    //{
                    //    float x = 0;
                    //    float y = 0;
                    //    float z = 0;
                    //
                    //    float a = 0;
                    //    float b = 0;
                    //    float c = 0;
                    //
                    //    for(int i = 0; i < 30; i ++)
                    //    {
                    //        x += localVertices[3*i];
                    //        y += localVertices[3*i +1];
                    //        z += localVertices[3*i +2];
                    //
                    //        a += receivedVertices[3*i];
                    //        b += receivedVertices[3*i +1];
                    //        c += receivedVertices[3*i +2];
                    //    }
                    //    x /= 30;
                    //    y /= 30;
                    //    z /= 30;
                    //    a /= 30;
                    //    b /= 30;
                    //    c /= 30;
                    //   angles.Add(Vector3.Angle(new Vector3(x,y,z),  new Vector3(a,b,c)));
                    //}
                }
                if(receivedMessage.Type == MessageType.Score)
                {
                    // Add the received score
                    angleScoreCalculator.AddAngleScore(receivedMessage.Score);
                    Debug.Log("Received Score");
                }
                if(receivedMessage.Type == MessageType.Sync)
                {
                    // Update the frame offset
                    stepDelta = step - receivedMessage.Step;
                    Debug.Log("Received Sync");
                }
                if(receivedMessage.Type == MessageType.Reset)
                {
                    // Perform a reset
                    Debug.Log("Received Reset");
                    Reset();
                }
            }
        }
    }
    /// <summary>
    /// Writes a csv file of the angle between average vertices for the evaluation
    /// </summary>
 //   void WriteCsv()
 //   {
 //       if (File.Exists(filePath))
 //       {
 //          File.Delete(filePath);
 //       }
 //
 //       using var fs = File.Open(filePath, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Write);
 //       using(var w = new StreamWriter(fs))
 //       {
 //           for (int i = 0; i < angles.Count; i ++)
 //           {
 //              var angle = angles[i];
 //               var line = angle.ToString("0.0000");
 //               w.WriteLine(line);
 //               w.Flush();
 //           }
 //       }
 //   }

     void OnDestroy()
    {
        m_MCSession.Dispose();
    }
#endif
}
