﻿using UnityEngine;
using UnityEngine.XR.ARFoundation;
using System.Collections;
using Unity.Collections;
using System.Collections.Generic;
using Logger = UnityEngine.XR.ARFoundation.Samples.Logger;
using UnityEngine.XR.ARFoundation.Samples;
using System.IO;
using UnityEngine.UI;

#if UNITY_IOS && !UNITY_EDITOR
using Unity.iOS.Multipeer;
using UnityEngine.XR.ARKit;
using System.Collections;
using Unity.Collections;
#endif

/// <summary>
/// Class responsible for the communication, as well as for controlling the components necessary for the simulation.
/// </summary>
public class CollaborativeSessionAS : MonoBehaviour
{
    /// <summary>
    /// The number of devices in the session
    /// </summary>
    int connectedPeerCount;
    /// <summary>
    /// Joint positions
    /// </summary>
    Vector3[] positions;
    /// <summary>
    /// Keeps track of the frames
    /// </summary>
    int step;
    /// <summary>
    /// Keeps track of the number of tracking data messages received
    /// </summary>
    int receivedTrackingData;
    /// <summary>
    /// List of joint positions tracked locally, used for the evaluation
    /// </summary>
    List<Vector3[]> localPositionsList;
    /// <summary>
    /// List of joint positions calculated from the relative positions, used for the evaluation
    /// </summary>
    List<Vector3[]> receivedPositionsList;
    /// <summary>
    /// Responsible for the body tracking
    /// </summary>
    HumanBodyTracker humanBodyTracker;
    /// <summary>
    /// Controls the muscle model
    /// </summary>
    MuscleCtrl muscleCtrl;
    /// <summary>
    /// Calculates the angle score and maintains the angle score list
    /// </summary>
    AngleScoreCalculator angleScoreCalculator;
    /// <summary>
    /// Text assets used to display information to the user
    /// </summary>
    public Text receivedTrackingText;
    public Text angleScoreText;
    public Text orderText;
    /// <summary>
    /// Path where evaluation data is saved
    /// </summary>
    private string filePath => Path.Combine(Application.persistentDataPath, "positionsFile.txt");

    /// <summary>
    /// The name for this network service.
    /// See <a href="https://developer.apple.com/documentation/multipeerconnectivity/mcnearbyserviceadvertiser">MCNearbyServiceAdvertiser</a>
    /// for the purpose of and restrictions on this name.
    /// </summary>
    public string serviceType
    {
        get => m_ServiceType;
        set => m_ServiceType = value;
    }
    [SerializeField]
    [Tooltip("The name for this network service. It should be 15 characters or less and can contain ASCII, lowercase letters, numbers, and hyphens.")]
    string m_ServiceType;


    void DisableNotSupported(string reason)
    {
        enabled = false;
    }

    void OnEnable()
    {
        humanBodyTracker = GetComponent<HumanBodyTracker>();
        muscleCtrl = GetComponent<MuscleCtrl>();
        angleScoreCalculator = new();
        // Multipeer is only supported on iOS
#if UNITY_IOS && !UNITY_EDITOR
        m_MCSession.Enabled = true;
#else
        DisableNotSupported("Collaborative sessions are an ARKit 3 feature; This platform does not support them.");
#endif
    }

    void Start()
    {
        localPositionsList = new();
        receivedPositionsList = new();
        connectedPeerCount = 0;
        step = 0;
        receivedTrackingData = 0;
    }
    // Multipeer is only supported on iOS
#if UNITY_IOS && !UNITY_EDITOR
    /// <summary>
    /// The multipeer session
    /// </summary>
    MCSession m_MCSession;

    void OnDisable()
    {
        m_MCSession.Enabled = false;
    }

    void Awake()
    {
        m_MCSession = new MCSession(SystemInfo.deviceName, m_ServiceType);
    }

    void Update()
    {
        step += 1;
        // Displaying information
        receivedTrackingText.text = receivedTrackingData.ToString();
        orderText.text = angleScoreCalculator.angleScoreOrder.ToString();
        // Angle score is updated every 30 frames
        if(step % 30 == 0)
        {
            angleScoreCalculator.UpdateAngleScore(humanBodyTracker.boneController.skeletonRoot);
            angleScoreText.text = angleScoreCalculator.angleScore.ToString();
        }
        // The first 1000 steps are recorded
        if(receivedTrackingData == 1000)
        {
            WriteCsv();
        }
        // Perform reset if network structure changes
        if (connectedPeerCount != m_MCSession.ConnectedPeerCount)
        {
            connectedPeerCount = m_MCSession.ConnectedPeerCount;
            Reset();
        }

        if (m_MCSession.ConnectedPeerCount != 0) // If other devices are connected
        {
            // Check if new angle score was received and determine angle score order again
            CheckIncomingData();
            angleScoreCalculator.DetermineAngleScoreOrder();

            if(angleScoreCalculator.angleScoreOrder == 0) // This code block is only performed by the main device
            {
                // Get the most recent positions, update the arm model, then send the relative positions
                positions = humanBodyTracker.GetPositions();
                muscleCtrl.UpdateArm(positions[0], positions[1], positions[2]);
                SendTrackingData();
            }
            // This next code block is performed by all devices
            // Send reset message and perform reset every 30 frames
            if(step % 30 == 0)
            {
                SendReset();
                Reset();
            }
            CheckIncomingData();
        }
        else // If no other devices are connected
        {
            positions = humanBodyTracker.GetPositions();
            muscleCtrl.UpdateArm(positions[0], positions[1], positions[2]);
        }
    }
    /// <summary>
    /// Calls for the reset of the angle score calculator, which is described in the AngleScoreCalculator class.
    /// </summary>
    void Reset()
    {
        angleScoreCalculator.ResetAngleScores(humanBodyTracker.boneController.skeletonRoot);
        angleScoreText.text = angleScoreCalculator.angleScore.ToString();
        SendScore();
    }
    /// <summary>
    /// Sends a message containing the relative joint positions
    /// </summary>
    void SendTrackingData()
    {
        SendPositions(RelativePositions(positions));
    }
    /// <summary>
    /// Returns the joint positions relative to the root
    /// </summary>
    Vector3[] RelativePositions(Vector3[] positions)
    {
        Vector3[] relativePositions = positions;
        for(int i = 0; i < 3; i++)
        {
            // The joint positions are transformed from world coordinates to skeletonRoot local coordinates
            relativePositions[i] = humanBodyTracker.boneController.skeletonRoot.InverseTransformPoint(relativePositions[i]);
        }
        return relativePositions;
    }
    /// <summary>
    /// Sends a message containing the angle score
    /// </summary>
    void SendScore()
    {
        Message message = new Message();
        message.Type = MessageType.Score;
        message.Score = angleScoreCalculator.angleScore;
        SendMessage(message);
    }
    /// <summary>
    /// Sends a message that triggers a reset
    /// </summary>
    void SendReset()
    {
        Message message = new Message();
        message.Type = MessageType.Reset;
        SendMessage(message);
    }
    /// <summary>
    /// Sends a message that contains the relative joint positions
    /// </summary>
    void SendPositions(Vector3[] positions)
    {
        Message message = new Message();
        message.Type = MessageType.TrackingData;
        message.Positions = positions;
        SendMessage(message);
    }
    /// <summary>
    /// Converts a message object to json, to NSData which is sent to the devices in the multipeer session
    /// </summary>
    void SendMessage(Message message)
    {
        string jsonString = JsonUtility.ToJson(message);
        NativeArray<byte> bytes = new NativeArray<byte>(System.Text.Encoding.UTF8.GetBytes(jsonString), Allocator.Temp);
        NativeSlice<byte> bytesSlice = new NativeSlice<byte>(bytes);
        using (var data = NSData.CreateWithBytesNoCopy(bytesSlice))
        {
            m_MCSession.SendToAllPeers(data, MCSessionSendDataMode.Reliable);
        }
    }
    /// <summary>
    /// Checks the incoming data for messages and performs a specific action for each message type
    /// </summary>
    void CheckIncomingData()
    {
        while (m_MCSession.ReceivedDataQueueSize > 0)
            {
            using (var data = m_MCSession.DequeueReceivedData())
            {
                // Construct message object from received NSData
                var slice = data.Bytes;
                var bytes = new byte[slice.Length];
                slice.CopyTo(bytes);
                string receivedMessageJson = System.Text.Encoding.UTF8.GetString(bytes);
                Message receivedMessage = JsonUtility.FromJson<Message>(receivedMessageJson);

                if (receivedMessage.Type == MessageType.Score)
                {
                    // Add the received score
                    angleScoreCalculator.AddAngleScore(receivedMessage.Score);
                }
                if (receivedMessage.Type == MessageType.Reset)
                {
                    // Perform a reset
                    Reset();
                }
                if (receivedMessage.Type == MessageType.TrackingData)
                {
                    receivedTrackingData += 1;
                    Vector3 rootPosition = humanBodyTracker.boneController.skeletonRoot.position;
                    Vector3[] receivedPositions = receivedMessage.Positions;
                    for(int i = 0; i < 3; i++)
                    {
                        // Joint positions are transformed from skeletonRoot local coordinates back to world coordinates
                        receivedPositions[i] = humanBodyTracker.boneController.skeletonRoot.TransformPoint(receivedPositions[i]);
                    }
                    // Update arm model with received positions
                    muscleCtrl.UpdateArm(receivedPositions[0], receivedPositions[1], receivedPositions[2]);
                    
                    // Store first 1000 local and received positions for the evaluation
                    if(receivedTrackingData < 1000)
                    {
                        localPositionsList.Add(humanBodyTracker.GetPositions());
                        receivedPositionsList.Add(receivedPositions);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Writes a csv file containing the local and received positions for the evaluation
    /// </summary>
    void WriteCsv()
    {
        if (File.Exists(filePath))
        {
            File.Delete(filePath);
        }

        using var fs = File.Open(filePath, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Write);

        using(var w = new StreamWriter(fs))
        {
            for (int i = 0; i < localPositionsList.Count; i ++)
            {
                var firstX = localPositionsList[i][0];
                var firstY = localPositionsList[i][1];
                var firstZ = localPositionsList[i][2];

                var secondX = receivedPositionsList[i][0];
                var secondY = receivedPositionsList[i][1];
                var secondZ = receivedPositionsList[i][2];

                var line = string.Format("{0},{1},{2},{3},{4},{5}", firstX, firstY, firstZ, secondX, secondY, secondZ);

                w.WriteLine(line);
                w.Flush();
            }
        }
    }

    void OnDestroy()
    {
        m_MCSession.Dispose();
    }
#endif
}

