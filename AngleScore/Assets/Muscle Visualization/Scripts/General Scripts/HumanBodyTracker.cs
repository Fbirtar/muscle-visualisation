﻿using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Unity.Barracuda;
using Unity.Collections;
using Unity.XR.CoreUtils;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARFoundation.Samples;
using UnityEngine.XR.ARSubsystems;

/// <summary>
/// Subscribes to events caused by the human body manager and updates joint positions accordingly.
/// Based on the HumanBodyTracker.cs script from ARFoundationSamples.
/// Used for the tracking distribution.
/// </summary>
public class HumanBodyTracker : MonoBehaviour
{
    [SerializeField]
    [Tooltip("The Skeleton prefab to be controlled.")]
    GameObject m_SkeletonPrefab;

    [SerializeField]
    [Tooltip("The ARHumanBodyManager which will produce body tracking events.")]
    ARHumanBodyManager m_HumanBodyManager;

    /// <summary>
    /// The joint positions
    /// </summary>
    Vector3[] positions = new Vector3[4];

    /// <summary>
    /// Get/Set the <c>ARHumanBodyManager</c>.
    /// </summary>
    public ARHumanBodyManager humanBodyManager
    {
        get { return m_HumanBodyManager; }
        set { m_HumanBodyManager = value; }
    }
    /// <summary>
    /// Get/Set the skeleton prefab.
    /// </summary>
    public GameObject skeletonPrefab
    {
        get { return m_SkeletonPrefab; }
        set { m_SkeletonPrefab = value; }
    }
    /// <summary>
    /// Sphere that shows the root position
    /// </summary>
    GameObject rootSphere;

    Dictionary<TrackableId, BoneController> m_SkeletonTracker = new Dictionary<TrackableId, BoneController>();

    #region yuxo
    [HideInInspector]
    public BoneController boneController;
    [HideInInspector]
    public bool isTracked;
    #endregion

    void OnEnable()
    {
        rootSphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        rootSphere.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        Debug.Log("Human body tracker OnEnable()");
        Debug.Assert(m_HumanBodyManager != null, "Human body manager is required.");
        m_HumanBodyManager.enabled = true;
        m_HumanBodyManager.humanBodiesChanged += OnHumanBodiesChanged;
    }

    void OnDisable()
    {
        if (m_HumanBodyManager != null)
            m_HumanBodyManager.humanBodiesChanged -= OnHumanBodiesChanged;
        m_HumanBodyManager.enabled = false;
    }
    /// <summary>
    /// Called on ARHumanBody Events
    /// </summary>
    void OnHumanBodiesChanged(ARHumanBodiesChangedEventArgs eventArgs)
    {
        foreach (var humanBody in eventArgs.added) // humanBody is detected for the first time
        {
            if (!m_SkeletonTracker.TryGetValue(humanBody.trackableId, out boneController))
            {
                Debug.Log($"Adding a new skeleton [{humanBody.trackableId}].");
                var newSkeletonGO = Instantiate(m_SkeletonPrefab, humanBody.transform);
                boneController = newSkeletonGO.GetComponent<BoneController>();
                m_SkeletonTracker.Add(humanBody.trackableId, boneController);
                isTracked = true;
            }

            boneController.InitializeSkeletonJoints();
            boneController.ApplyBodyPose(humanBody);
            UpdatePositions();
        }

        foreach (var humanBody in eventArgs.updated) // humanBody is updated each frame
        {
            if (m_SkeletonTracker.TryGetValue(humanBody.trackableId, out boneController))
            {
                boneController.ApplyBodyPose(humanBody);
            }
            UpdatePositions();
            rootSphere.transform.position = boneController.skeletonRoot.transform.position;
        }

        foreach (var humanBody in eventArgs.removed) // humanBody is removed
        {
            Debug.Log($"Removing a skeleton [{humanBody.trackableId}].");
            if (m_SkeletonTracker.TryGetValue(humanBody.trackableId, out boneController))
            {
                Destroy(boneController.gameObject);
                m_SkeletonTracker.Remove(humanBody.trackableId);
                isTracked = false;
            }
        }
    }
    /// <summary>
    /// Updates the joint positions necessary for the simulation.
    /// </summary>
    private void UpdatePositions()
    {
        Vector3 shoulderPosition = boneController.GetShoulder();
        Vector3 elbowPosition = boneController.GetElbow();
        Vector3 wristPosition = boneController.GetWrist();
        Vector3 rightShoulderPosition = boneController.GetRightShoulderPosition();
        positions[0] = shoulderPosition;
        positions[1] = elbowPosition;
        positions[2] = wristPosition;
        positions[3] = rightShoulderPosition;
    }
    /// <summary>
    /// Returns an array of the joint positions
    /// </summary>
    public Vector3[] GetPositions()
    {
        return positions;
    }
}
