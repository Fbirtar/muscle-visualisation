using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using Logger = UnityEngine.XR.ARFoundation.Samples.Logger;

/// <summary>
/// Controls the anchors.
/// Not used in the experiments of the thesis.
/// </summary>
public class AnchorCtrl : MonoBehaviour
{
    /// <summary>
    /// The anchor manager that triggers anchor events
    /// </summary>
    public ARAnchorManager m_AnchorManager;
    /// <summary>
    /// List of the anchors
    /// </summary>
    public List<ARAnchor> m_Anchors = new();

    private void OnEnable()
    {
        m_AnchorManager.anchorsChanged += OnAnchorsChanged;
    }

    void OnDisable()
    {
        m_AnchorManager.anchorsChanged -= OnAnchorsChanged;
    }
    /// <summary>
    /// Creates new anchor in front of the device
    /// </summary>
    public void CreateAnchor()
    {
        Vector3 position = Vector3.zero;
        position.z = 2;
        Quaternion rotation = Quaternion.identity;
        var gameObject = Instantiate(m_AnchorManager.anchorPrefab, position, rotation);
        ARAnchor anchor = gameObject.AddComponent<ARAnchor>();
        anchor.transform.Rotate(0, 95, -10);
        if (anchor != null)
        {
            // Remember the anchor so we can remove it later.
            m_Anchors.Add(anchor);
            Logger.Log("Created Anchor");
        }
    }
    /// <summary>
    /// Removes and destroys all anchor objects. Anchor objects use a lot of resources and should always destroyed if not used.
    /// </summary>
    public void RemoveAllAnchors()
    {
        foreach (var anchor in m_Anchors)
        {
            Destroy(anchor.gameObject);
        }
        m_Anchors.Clear();
        Logger.Log("Anchors have been removed");
    }
    /// <summary>
    /// Called on anchor events
    /// </summary>
    void OnAnchorsChanged(ARAnchorsChangedEventArgs eventArgs)
    {
        foreach (var anchor in eventArgs.added)
        {
            m_Anchors.Add(anchor);
        }
        foreach (var anchor in eventArgs.removed)
        {
            m_Anchors.Remove(anchor);
        }
    }
    /// <summary>
    /// Returns the position of the anchor used as a reference point for the relative positions
    /// </summary>
    public Vector3 GetAnchorPosition()
    {
        return m_Anchors[0].transform.position;
    }
}
