using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class used for communication
/// </summary>
public class Message
{
    public MessageType Type;
    public double Score;
    public Vector3[] Positions = new Vector3[3];
    public float[] Vertices = new float[5];
    public int Step;
}
