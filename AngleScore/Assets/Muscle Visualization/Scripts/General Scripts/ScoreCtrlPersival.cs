using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Responsible for maintaining the device score list.
/// Used for the vertex distribution.
/// </summary>
public class ScoreCtrlPersival : MonoBehaviour
{
    /// <summary>
    /// Responsible for calculating the device score
    /// </summary>
    ScoreCalculator scoreCalculator;
    /// <summary>
    /// The device score list
    /// </summary>
    [HideInInspector]
    List<double> scoreList = new();
    /// <summary>
    /// This device's score
    /// </summary>
    [HideInInspector]
    public double score;
    [HideInInspector]
    /// <summary>
    /// This device's score order
    /// </summary>
    public int scoreOrder;

    void Start()
    {
        scoreCalculator = new ScoreCalculator();
        UpdateScore();
    }
    /// <summary>
    /// This class is unmodified from the persival deformation application.
    /// </summary>
    public void UpdateScore()
    {
        score = scoreCalculator.CalculateScore();
        scoreList.Add(this.score);
    }
    /// <summary>
    /// Determines this device's score order
    /// </summary>
    public void DetermineScoreOrder()
    {
        scoreList.Sort();
        scoreList.Reverse();
        for (int i = 0; i < scoreList.Count; i++)
        {
            if (scoreList[i] == score)
            {
                scoreOrder = i;
            }
        }
    }
    /// <summary>
    /// Clears the score list and updates the local score
    /// </summary>
    public void ResetScores()
    {
        Debug.Log("Resetting the scores");
        scoreList.Clear();
        UpdateScore();
    }
    /// <summary>
    /// Adds the given score to the score list
    /// </summary>
    public void AddScore(double score)
    {
        scoreList.Add(score);
    }
}
