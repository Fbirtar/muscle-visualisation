using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Logger = UnityEngine.XR.ARFoundation.Samples.Logger;
/// <summary>
/// Responsible for calculating the angle score and maintaining the angle score list
/// </summary>
public class AngleScoreCalculator{
    /// <summary>
    /// List of angle scores of all connected devices
    /// </summary>
    List<double> angleScoreList;
    /// <summary>
    /// This device's angle score
    /// </summary>
    public double angleScore;
    /// <summary>
    /// This device's angle score order
    /// </summary>
    public int angleScoreOrder;
    /// <summary>
    /// The position of the main camera will be used for the angle score calculation
    /// </summary>
    Camera mainCamera;
    /// <summary>
    /// Tiebreaker score
    /// </summary>
    double scoreFromDeviceId;

    /// <summary>
    /// Constructor 
    /// </summary>
    public AngleScoreCalculator()
    {
        angleScore = 0;
        angleScoreList = new List<double>();
        mainCamera = Camera.main;
        scoreFromDeviceId = ScoreFromDeviceId();
    }
    /// <summary>
    /// Calculates the angle score, takes the root joint transform as input
    /// </summary>
    public double CalculateAngleScore(Transform root)
    {
        // These vectors define the coordinate origin of the root coordinate system
        Vector3 negativeZ = -root.forward;
        Vector3 y = root.up;
        Vector3 x = root.right;

        Vector3 cameraPosition = mainCamera.transform.position;
        // Vector from the root joint to the camera position
        Vector3 rootToCamera = cameraPosition - root.position;
        // Root-to-camera vector projected on the plane defined through vectors y and x.
        Vector3 v = rootToCamera - (Vector3.Dot(rootToCamera, y) * y);
        Vector3 u = negativeZ * Vector3.Magnitude(v);
        float angle;
        if(Vector3.Dot(x, v) >= 0) // If the dot product of x and v is positive, v lies in the right half of the circle
        {
            angle = Vector3.Angle(u, v);
        }
        else
        {
            angle = 360 - Vector3.Angle(u, v); // If the dot product of x and v is negative, v lies in the left half of the circle
        }
        double score = GetScoreFromAngle(angle);
        // Adding tiebreaker score
        score += scoreFromDeviceId;
        return score;
    }
    /// <summary>
    /// Assigns a score based on the angle at which the device finds itself in relation
    /// to the tracked person, as described in the thesis
    /// </summary>
    double GetScoreFromAngle(float angle)
    {
        if (0 <= angle && angle < 40) return 4;
        else if (40 <= angle && angle < 100) return 5;
        else if (100 <= angle && angle < 140) return 2;
        else if (140 <= angle && angle < 310) return 1;
        else return 3;
    }
    /// <summary>
    /// Clears the angle score list and updates the angle score.
    /// </summary>
    public void ResetAngleScores(Transform root)
    {
        angleScoreList.Clear();
        UpdateAngleScore(root);
    }
    /// <summary>
    /// Removes this device's angle score from the list, calculates a new score,
    /// then adds it to the score list.
    /// </summary>
    public void UpdateAngleScore(Transform root)
    {
        if(root is not null)
        {
            angleScoreList.Remove(angleScore);
            angleScore = CalculateAngleScore(root);
            angleScoreList.Add(angleScore);
        }
        else
        {
            angleScoreList.Remove(angleScore);
            angleScore = 0;
            angleScoreList.Add(angleScore);
        }
    }
    /// <summary>
    /// Determines this device's angle score order
    /// </summary>
    public void DetermineAngleScoreOrder()
    {
        angleScoreList.Sort();
        angleScoreList.Reverse();
        for (int i = 0; i < angleScoreList.Count; i++)
        {
            if (angleScoreList[i] == angleScore)
            {
                angleScoreOrder = i;
            }
        }
    }
    /// <summary>
    /// Adds the given score to the score list
    /// </summary>
    public void AddAngleScore(double angleScore)
    {
        if (!angleScoreList.Contains(angleScore))
        {
            angleScoreList.Add(angleScore);
        }
    }
    /// <summary>
    /// Remaps given value which lies in the range low1-high1 to the given range of low2-high2
    /// </summary>
    double Remap(float value, float low1, float high1, float low2, float high2)
    {
        double result = low1 + (value - low1) * (high2 - low2) / (high1 - low1) ;
        return result;
    }
    /// <summary>
    /// Calculates a tiebreaker score between 0 and 0.5 based on a unique device id
    /// </summary>
    double ScoreFromDeviceId()
    {
        string deviceID = SystemInfo.deviceUniqueIdentifier;
        char[] charArray = deviceID.ToCharArray();
        int sum = 0;
        for (int i = 0; i < charArray.Length; i++)
        {
            sum += ToInt(charArray[i]);
        }
        sum = sum / charArray.Length;
        double result = Remap(sum, 0, 150, 0f, 0.5f); // 150 is the maximum value a char from the id can take
        return result;
    }
    /// <summary>
    /// Converts a char to an int
    /// </summary>
    int ToInt(char c)
    {
        return (int)(c - '0');
    }
}
