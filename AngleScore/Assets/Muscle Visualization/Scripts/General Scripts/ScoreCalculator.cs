using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Text.RegularExpressions;
/// <summary>
/// This class is responsible for calculating the device score
/// </summary>
public class ScoreCalculator
{
    /// <summary>
    /// Csv file containing device ids and their benchmark ratings
    /// </summary>
    public TextAsset iOSDevices;
    /// <summary>
    /// Tiebreaker score
    /// </summary>
    double scoreFromDeviceId;
    /// <summary>
    /// The device model id
    /// </summary>
    public String Id
    {
        get; set;
    }
    /// <summary>
    /// Geekbench score
    /// </summary>
    public int GeekbenchScore
    {
        get; private set;
    }
    /// <summary>
    /// Geekbench machine learning score
    /// </summary>
    public int MLScore
    {
        get; private set;
    }
    /// <summary>
    /// Passmark memory score
    /// </summary>
    public int MemScore
    {
        get; private set;
    }
    /// <summary>
    /// The max and min values of each benchmark rating
    /// </summary>
    readonly int maxGeekbenchScore = 1722;
    readonly int minGeekbenchScore = 352;
    readonly int maxMLScore = 1013;
    readonly int minMLScore = 181;
    readonly int maxMemScore = 59806;
    readonly int minMemScore = 10424;
    /// <summary>
    /// The coefficients of the GeekbenchScore, the MLScore, and the MemScore (in order)
    /// </summary>
    readonly float alpha = 0.5f;
    readonly float beta = 2f;
    readonly float gamma = 0.5f;

    /// <summary>
    /// Constructor 
    /// </summary>
    public ScoreCalculator()
    {
        string deviceModel = SystemInfo.deviceModel;
        Id = deviceModel.Replace(",", ".");
        scoreFromDeviceId = ScoreFromDeviceId();
        ReadCSV();
    }
    /// <summary>
    /// Reads the csv file and retrieves this device's benchmark ratings
    /// </summary>
    void ReadCSV()
    {
        GeekbenchScore = 0;
        MLScore = 0;
        MemScore = 0;
        iOSDevices = Resources.Load("iOSdevices") as TextAsset;
        string text = iOSDevices.text;
        string[] textLines = Regex.Split(text, "\n|\r|\r\n");
        for (int i = 0; i < textLines.Length; i++)
        {
            string line = textLines[i];
            var values = line.Split(",");
            if (values[0].ToString() == Id)
            {
                GeekbenchScore = Int32.Parse(values[1]);
                MLScore = Int32.Parse(values[2]);
                MemScore = Int32.Parse(values[3]);
                break;
            }
        }
    }
    /// <summary>
    /// Calculates the device score
    /// </summary>
    public double CalculateScore()
    {
        double result;
        float batteryScore;
        if (SystemInfo.batteryStatus == BatteryStatus.Charging) batteryScore = 100; // If device is plugged in, battery level is irrelevant
        else batteryScore = SystemInfo.batteryLevel * 100; // Map battery level to 0-100 range
        if (batteryScore <= 20) result = 0; // In low power mode the device score is set to 0
        else
        {
            // Weighted average of the remapped scores from each category, multiplied by the coefficients
            result = batteryScore + alpha * Remap(GeekbenchScore, minGeekbenchScore, maxGeekbenchScore, 0, 100) +
            beta * Remap(MLScore, minMLScore, maxMLScore, 0, 100) + gamma * Remap(MemScore, minMemScore, maxMemScore, 0, 100);
            result /= 4;
            result = Mathf.Round((float)result);
            result += scoreFromDeviceId;
        }
        return result;
    }
    /// <summary>
    /// Remaps given value which lies in the range low1-high1 to the given range of low2-high2
    /// </summary>
    double Remap(float value, float low1, float high1, float low2, float high2)
    {
        double result = low1 + (value - low1) * (high2 - low2) / (high1 - low1);
        return result;
    }
    /// <summary>
    /// Calculates a tiebreaker score between 0 and 0.5 based on a unique device id
    /// </summary>
    double ScoreFromDeviceId()
    {
        string deviceID = SystemInfo.deviceUniqueIdentifier;
        char[] charArray = deviceID.ToCharArray();
        int sum = 0;
        for (int i = 0; i < charArray.Length; i++)
        {
            sum += ToInt(charArray[i]);
        }
        sum = sum / charArray.Length;
        double result = Remap(sum, 0, 150, 0f, 0.5f); // 150 is the maximum value a char from the id can take
        return result;
    }
    /// <summary>
    /// Converts a char to an int
    /// </summary>
    int ToInt(char c)
    {
        return (int)(c - '0');
    }
}
