using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity.Barracuda;
using UnityEngine;
/// <summary>
/// This class represents the neuronal network that calculates the activation values
/// </summary>
public class LocalNN
{
    /// <summary>
    /// Barracuda adapter
    /// </summary>
    private readonly LocalBarracudaAdapter barracudaAdapter;
    public LocalNN(NNModel modelAsset)
    {
        Debug.Log("Creation of Distributed NN....");
        barracudaAdapter = new LocalBarracudaAdapter(modelAsset);
        Debug.Log("Local NN has been built");
    }
    /// <summary>
    /// Calls the predict method of the barracuda adapter
    /// </summary>
    public float[] Predict(float[] input)
    {
        return barracudaAdapter.Predict(input);
    }
}
