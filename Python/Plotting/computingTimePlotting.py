
import matplotlib.pyplot as plt
import csv

ipadPluggedIn = []
iphonePluggedIn = []
iphoneLowPower = []
steps = []
averageCompTime = []

def read():
  i = 0
  averageIpad = 0
  averageIphone = 0
  averageIphoneLowPower = 0

  with open("ipadCompTime75.txt", 'r') as file:
    csvreader = csv.reader(file)
    for row in csvreader:
      averageIpad += float(row[0])
      ipadPluggedIn.append(float(row[0]))
      steps.append(i)
      i = i + 1
  with open("iphoneCompTime75.txt", 'r') as file:
    csvreader = csv.reader(file)
    for row in csvreader:
      iphonePluggedIn.append(float(row[0]))
      averageIphone += float(row[0])
  with open("iphoneLowPower.txt", 'r') as file:
    csvreader = csv.reader(file)
    for row in csvreader:
      iphoneLowPower.append(float(row[0]))
      averageIphoneLowPower += float(row[0])

  averageCompTime.append(averageIpad/1000)
  averageCompTime.append(averageIphone/1000)
  averageCompTime.append(averageIphoneLowPower/1000)



def main():
  read()
  plt.plot(steps, ipadPluggedIn, label='iPad 75% battery', color='coral')
  plt.plot(steps, iphonePluggedIn, label='iPhone 75% battery', color='crimson')
  plt.plot(steps, iphoneLowPower,  label='iPhone Low Power Mode', color='royalblue')
  plt.legend()
  plt.ylabel('Computation time (ms)')
  plt.xlabel('Step')
  plt.title('Comparison between computation times')
  plt.show()

def average():
  read()
  devices = ['iPad', 'iPhone', 'iPhone Low Power Mode']
  plt.bar(devices, averageCompTime, width=0.4, color='darkorange')
  plt.ylabel('Computation time (ms)')
  plt.title('Average computation time')
  plt.show()

plt.rcParams.update({'font.size': 22})
average()