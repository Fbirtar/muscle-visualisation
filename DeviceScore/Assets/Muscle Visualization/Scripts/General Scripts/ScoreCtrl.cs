using System.Collections.Generic;
using UnityEngine;
using Logger = UnityEngine.XR.ARFoundation.Samples.Logger;

public class ScoreCtrl : MonoBehaviour
{
    ScoreCalculator scoreCalculator;
    AnchorCtrl anchorCtrl;
    [HideInInspector]
    List<double> scoreList = new();
    [HideInInspector]
    public double score;
    [HideInInspector]
    public int scoreOrder = 8;

    public TextAsset iOSDevices;

    private void OnEnable()
    {
        anchorCtrl = GetComponent<AnchorCtrl>();
    }

    void Start()
    {
        scoreCalculator = new ScoreCalculator();
        UpdateScore();
    }

    public void UpdateScore()
    {
        score = scoreCalculator.CalculateScore();
        scoreList.Add(this.score);
    }

    public void DetermineScoreOrder()
    {
        scoreList.Sort();
        scoreList.Reverse();
        for (int i = 0; i < scoreList.Count; i++)
        {
            if (scoreList[i] == score)
            { 
                this.scoreOrder = i;
            }
        }
    }

    public void ResetScores()
    {
        Logger.Log("Resetting the scores");
        anchorCtrl.RemoveAllAnchors();
        scoreList.Clear();
        UpdateScore();
    }

    public void AddScore(double score)
    {
        scoreList.Add(score);
    }
}