using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This class is unmodified from the persival deformation application.
/// </summary>
public class UIManager : MonoBehaviour
{
    /// <summary>
    /// the toggle to open debug menu
    /// </summary>
    public Toggle Tg_Menu;
    /// <summary>
    /// to show the tracking result
    /// </summary>
    public Toggle Tg_Track;
    /// <summary>
    /// to activate the acitvation model
    /// </summary>
    public Toggle Tg_ActiModel;
    /// <summary>
    /// to activate the deformation model
    /// </summary>
    public Toggle Tg_MeshModel;
    /// <summary>
    /// to activate the rendering solution
    /// </summary>
    public Toggle Tg_RenderingMesh;
    /// <summary>
    /// to change the simulated weight in hand
    /// </summary>
    public Slider Sli_Weight;
    /// <summary>
    /// to visualize the current simulated wright
    /// </summary>
    public Text Txt_Weight;
    /// <summary>
    /// the panel of debugging UI
    /// </summary>
    public Image panel;
    /// <summary>
    /// to output some debug data
    /// </summary>
    public Text Txt_Debug;
    /// <summary>
    /// to switch on/off the color shading of biceps
    /// </summary>
    public Toggle Tg_ColorShading;
    /// <summary>
    /// the rotation angle of biceps around the upper-arm bone
    /// </summary>
    public Slider sli_angle;
    /// <summary>
    /// to visualize the value of sli_angle
    /// </summary>
    public Text txt_angle;
    /// <summary>
    /// whether to use full model
    /// </summary>
    public Toggle tg_isFullModel;

    /// <summary>
    /// initialization
    /// </summary>
    public void f_Init()
    {
        Tg_MeshModel.gameObject.SetActive(false);
        Tg_RenderingMesh.gameObject.SetActive(false);
        Sli_Weight.onValueChanged.AddListener(f_Sli_Weight);
        Tg_Menu.onValueChanged.AddListener(f_Tg_Menu);
        Tg_ActiModel.onValueChanged.AddListener(f_Tg_ActiModel);
        Tg_MeshModel.onValueChanged.AddListener(f_Tg_MeshModel);
        Tg_RenderingMesh.onValueChanged.AddListener(f_Tg_Rendering);
        sli_angle.onValueChanged.AddListener(f_Sli_Angle);
        sli_angle.value = 270;
    }

   
    private void f_Tg_Rendering(bool isOn)
    {
        //GlobalCtrl.M_PartiVisualizer.ChangeStatus(isOn);
        GlobalCtrl.M_FaceVisualizer.ChangeStatus(isOn);
    }
    private void f_Tg_ActiModel(bool isOn)
    {
        GlobalCtrl.M_MuscleActiCtrl.f_Init();
        Tg_MeshModel.gameObject.SetActive(true);
        if (isOn)
            Tg_ActiModel.interactable = false;
    }
    private void f_Tg_MeshModel(bool isOn)
    {
        GlobalCtrl.M_MeshCtrl.f_Init(tg_isFullModel.isOn);
        Tg_RenderingMesh.gameObject.SetActive(true);
        tg_isFullModel.interactable = false;
        if (isOn)
            Tg_MeshModel.interactable = false;
    }
    public void f_Txt_Debug(string input)
    {
        Txt_Debug.text = input;
    }
    private void f_Tg_Menu(bool isOn)
    {
        panel.gameObject.SetActive(isOn);
        GlobalCtrl.M_FaceVisualizer.coordX.SetActive(isOn);
        GlobalCtrl.M_FaceVisualizer.coordY.SetActive(isOn);
        GlobalCtrl.M_FaceVisualizer.coordZ.SetActive(isOn);
        GlobalCtrl.M_FaceVisualizer.upper.SetActive(isOn);
        GlobalCtrl.M_FaceVisualizer.lower.SetActive(isOn);
    }
    public void f_Sli_Angle(float input)
    {
        txt_angle.text = input.ToString();
    }
    public void f_Sli_Weight(float input)
    {
        if (input < 5)
            GlobalCtrl.M_MuscleActiCtrl.SetWeight(0);
        else if (input < 10)
            GlobalCtrl.M_MuscleActiCtrl.SetWeight(5);
        else if (input < 20)
            GlobalCtrl.M_MuscleActiCtrl.SetWeight(10);
        else
            GlobalCtrl.M_MuscleActiCtrl.SetWeight(20);
        Txt_Weight.text = input.ToString();

    }

    /// <summary>
    /// I want to visualize the tracking status in real time, but I failed, can you fix that Filip?
    /// </summary>
    void LateUpdate()
    {
        Tg_Track.isOn = GlobalCtrl.M_Instance.M_HumanBodyTracker.isTracked;
    }
}
