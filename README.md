# Muscle Visualisation

Implementation for the bachelor thesis "Peer-to-Peer Distribution of Mobile Simulations" by Filip Emanuel Birtar

## Description:
The distribution of an iOS muscle visualization app that tracks the movement of a human body and shows the muscle contraction of the left arm.
There are two unity projects: in "AngleScore" the distribution is done using the angle score, while in "DeviceScore" the distribution is done using the device score.
The AngleScore project has two scenes corresponding to two experiments, one for the tracking distribution and one for the vertex distribution.
The DeviceScore project also has two scenes, however only one is relevant for the thesis, the DeformationScore scene which corresponds with the experiment for the 
vertex distribution by device score. 

## How to use:
1. Clone this repository
2. Open the desired project in unity
3. Go to File -> Build Settings, click Build and choose a build folder
4. Select a scene
5. Open the newly built Xcode project from the build folder and run it on an iPhone or iPad (minimum iOS version 11.0)

## Possible issue
Sometimes cloning the project leads to an issue with the Barracuda plug-in. This can be solved by opening the unity project (not in safe mode!) and updating the Barracuda package from the package manager.

