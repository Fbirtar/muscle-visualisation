
using Unity.Barracuda;
using UnityEngine;
/// <summary>
/// This class is responsible for updating the muscle model
/// </summary>
public class MuscleCtrl : MonoBehaviour
{
    /// <summary>
    /// This lifted weight
    /// </summary>
    public Weights weight = Weights.weight0;
    int inputWeight;
    /// <summary>
    /// This muscle activation values
    /// </summary>
    [HideInInspector]
    public float[] activations;
    /// <summary>
    /// Empty game objects positioned conveniently in unity in order to calculate the length of the two parts of the arm model.
    /// </summary>
    public GameObject upperArmElbow;
    public GameObject upperArmShoulder;
    public GameObject foreArmElbow;
    public GameObject foreArmWrist;
    /// <summary>
    /// Parent objects of the upperarm and forearm models
    /// </summary>
    public GameObject upperarm;
    public GameObject forearm;
    /// <summary>
    /// Gameobjects used for rotating the arm model
    /// </summary>
    public GameObject shoulderTarget;
    public GameObject elbowTarget;
    public GameObject wristTarget;
    /// <summary>
    /// Current elbow angle
    /// </summary>
    private float CurElbowAngle;
    /// <summary>
    /// Previous elbow angle, used to calculate velocity
    /// </summary>
    private float LastElbowAngle = 0;
    /// <summary>
    /// Angular velocity
    /// </summary>
    private float CurVelocity;
    /// <summary>
    /// Previous velocity, used to calculate acceleration 
    /// </summary>
    private float LastVelocity = 0;
    /// <summary>
    /// Angular acceleration 
    /// </summary>
    private float CurAcceleration;
    /// <summary>
    /// The materials of the three muscles which are colored
    /// </summary>
    public Material triceps1Material;
    public Material bicepsMaterial;
    public Material object4Material;
    /// <summary>
    /// The neuronal network calculating the activation values
    /// </summary>
    private LocalNN myNetwork;
    /// <summary>
    /// The neuronal model calculating the activation values
    /// </summary>
    public NNModel modelAsset;

    /// <summary>
    /// The initial lengths of the arm model, used for scaling.
    /// </summary>
    private float foreArmLength;
    private float upperArmLength;

    private void Awake()
    {
        inputWeight = (int)weight;
        foreArmLength = InitialForeArmLength();
        upperArmLength = InitialUpperArmLength();
        myNetwork = new LocalNN(modelAsset);
    }

    /// <summary>
    /// Updates the arm model using the joint positions in the parameters
    /// </summary>
    public void UpdateArm(Vector3 shoulderPosition, Vector3 elbowPosition, Vector3 wristPosition)
    {
        // Setting objects used for rotation
        shoulderTarget.transform.position = shoulderPosition;
        elbowTarget.transform.position = elbowPosition;
        wristTarget.transform.position = wristPosition;
        // Placing upperarm model
        upperarm.transform.position = (shoulderPosition + elbowPosition) / 2;
        upperarm.transform.localScale = Vector3.Distance(shoulderPosition, elbowPosition) / upperArmLength * Vector3.one;
        upperarm.transform.LookAt(elbowPosition, Vector3.Project(wristPosition - elbowPosition, elbowPosition - shoulderPosition)) ;
        // Placing forearm model
        forearm.transform.position = (wristPosition + elbowPosition) / 2;
        forearm.transform.localScale = Vector3.Distance(wristPosition, elbowPosition) / foreArmLength * Vector3.one;
        forearm.transform.LookAt(wristPosition);
        if (forearm.activeSelf && upperarm.activeSelf)
        {
            // Calculating the angle between the three positions
            CurElbowAngle = Vector3.Angle(wristTarget.transform.position - elbowTarget.transform.position, shoulderTarget.transform.position - elbowTarget.transform.position);
            // The velocity is the change of the angle
            CurVelocity = CurElbowAngle - LastElbowAngle;
            // The acceleration is the change of the velocity
            CurAcceleration = CurVelocity - LastVelocity;
            // Getting the activations of each muscle
            activations = GetActivationsFromData(new float[4] {
                inputWeight,
                CurElbowAngle,
                CurVelocity,
                CurAcceleration });
            // Color.Lerp linearly interpolates between the input colors by the activation int. 
            triceps1Material.color = Color.Lerp(
                Color.white,
                Color.blue,
                activations[0] * GetMaxActivation(inputWeight));

            bicepsMaterial.color = Color.Lerp(
                Color.white,
                Color.green,
                activations[1] * GetMaxActivation(inputWeight));

            object4Material.color = Color.Lerp(
                Color.white,
                Color.red,
                activations[2] * GetMaxActivation(inputWeight));
            LastElbowAngle = CurElbowAngle;
            LastVelocity = CurVelocity;
        }
    }
    /// <summary>
    /// Returns the initial length of the upperarm model.
    /// </summary>
    private float InitialUpperArmLength()
    {
        float result = Vector3.Distance(upperArmElbow.transform.position, upperArmShoulder.transform.position);
        return result;
    }
    /// <summary>
    /// Returns the initial length of the forearm model.
    /// </summary>
    private float InitialForeArmLength()
    {
        float result = Vector3.Distance(foreArmElbow.transform.position, foreArmWrist.transform.position);
        return result;
    }
    /// <summary>
    /// Returns the maximum possible activation depending on the weight.
    /// </summary>
    private float GetMaxActivation(int weight)
    {
        switch (weight)
        {
            case 0:
                return 0.4f;
            case 5:
                return 0.6f;
            case 10:
                return 0.8f;
            case 20:
                return 1.0f;
        }
        return 0.4f;
    }
    /// <summary>
    /// Calls the predict method of the neuronal network and returns the result.
    /// </summary>
    public float[] GetActivationsFromData(float[] input)
    {
        float weight = 0;
        switch ((int)input[0])
        {
            case 0:
                weight = 0;
                break;
            case 5:
                weight = 1;
                break;
            case 10:
                weight = 2;
                break;
            case 20:
                weight = 3;
                break;
        }
        float[] modelInput = { 1 - input[1] / 180, input[2] / 500, input[3] / 10000, weight };
        return myNetwork.Predict(modelInput);
    }
}
