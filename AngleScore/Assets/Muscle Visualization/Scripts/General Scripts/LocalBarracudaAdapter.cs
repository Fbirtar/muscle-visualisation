using UnityEngine;
using Unity.Barracuda;
/// <summary>
/// Barracuda enables the use of a neuronal network in onnx format in unity
/// </summary>
public class LocalBarracudaAdapter
{
    /// <summary>
    /// The runtime model
    /// </summary>
    private Model myRuntimeModel;
    /// <summary>
    /// The worker for the model
    /// </summary>
    private IWorker myWorker;
    /// <summary>
    /// Creates barracuda adapter using
    /// </summary>
    public LocalBarracudaAdapter(NNModel modelAsset)
    {
        Debug.Log("Loading local Model...");
        myRuntimeModel = ModelLoader.Load(modelAsset);
        myWorker = WorkerFactory.CreateWorker(WorkerFactory.Type.ComputePrecompiled, myRuntimeModel);
        Debug.Log("Loaded Barracuda Model");
    }

    private Tensor Tensorize(float[] input)
    {
        int[] shape = { -1, input.Length };
        Tensor tensor = new Tensor(shape, input);
        return tensor;
    }
    /// <summary>
    /// Neuronal network inference
    /// </summary>
    public float[] Predict(float[] input)
    {
        Tensor inTensor = Tensorize(input);
        Tensor outTensor = myWorker.Execute(inTensor).PeekOutput();
        new WaitForCompletion(outTensor);
        inTensor.Dispose();
        return outTensor.AsFloats();
    }
}
