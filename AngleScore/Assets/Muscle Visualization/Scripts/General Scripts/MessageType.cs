using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// The message types as described in the thesis
/// </summary>
public enum MessageType
{
   Reset, Score, TrackingData, Vertices, Sync
}
