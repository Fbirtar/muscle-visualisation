/// <summary>
/// The weight lifted by the observed person
/// </summary>
public enum Weights : int
{
    weight0 = 0,
    weight5 = 5,
    weight10 = 10,
    weight20 = 20,
}