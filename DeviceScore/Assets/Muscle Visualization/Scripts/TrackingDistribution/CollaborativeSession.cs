﻿using UnityEngine;
using UnityEngine.XR.ARFoundation;
using System.Collections;
using Unity.Collections;
using System.Collections.Generic;
using Logger = UnityEngine.XR.ARFoundation.Samples.Logger;
using UnityEngine.XR.ARFoundation.Samples;

#if UNITY_IOS && !UNITY_EDITOR
using Unity.iOS.Multipeer;
using UnityEngine.XR.ARKit;
using System.Collections;
using Unity.Collections;
#endif

/// <summary>
/// Class responsible managing all the components necessary for the tracking distribution using anchors.
/// It is based on the CollaborativeSession.cs script from the ARFoundation samples.
/// Not used in the experiments of the thesis.
/// </summary>
namespace UnityEngine.XR.ARFoundation.Samples
{
    public class CollaborativeSession : MonoBehaviour
    {
        /// <summary>
        /// The name for this network service.
        /// See <a href="https://developer.apple.com/documentation/multipeerconnectivity/mcnearbyserviceadvertiser">MCNearbyServiceAdvertiser</a>
        /// for the purpose of and restrictions on this name.
        /// </summary>
        public string serviceType
        {
            get => m_ServiceType;
            set => m_ServiceType = value;
        }
        [SerializeField]
        [Tooltip("The name for this network service. It should be 15 characters or less and can contain ASCII, lowercase letters, numbers, and hyphens.")]
        string m_ServiceType;
        /// <summary>
        /// Number of connected peers
        /// </summary>
        int connectedPeerCount;
        /// <summary>
        /// Number of connected peers
        /// </summary>
        Vector3[] positions;
        /// <summary>
        /// Updates the joint positions on human body events
        /// </summary>
        HumanBodyTracker humanBodyTracker;
        /// <summary>
        /// Controls the arm model
        /// </summary>
        MuscleCtrl muscleCtrl;
        /// <summary>
        /// Controls the anchor used as a reference point
        /// </summary>
        AnchorCtrl anchorCtrl;
        /// <summary>
        /// Maintains the score list
        /// </summary>
        ScoreCtrl scoreCtrl;
        /// <summary>
        /// Manager for the feature points
        /// </summary>
        public ARPointCloudManager pointCloudManager;
        /// <summary>
        /// The ARSession whose data is shared
        /// </summary>
        ARSession m_ARSession;

        void DisableNotSupported(string reason)
        {
            enabled = false;
            Logger.Log(reason);
        }

        void OnEnable()
        {
            humanBodyTracker = GetComponent<HumanBodyTracker>();
            muscleCtrl = GetComponent<MuscleCtrl>();
            anchorCtrl = GetComponent<AnchorCtrl>();
            scoreCtrl = GetComponent<ScoreCtrl>();
            humanBodyTracker.enabled = false;

#if UNITY_IOS && !UNITY_EDITOR
            var subsystem = GetSubsystem();
            if (!ARKitSessionSubsystem.supportsCollaboration || subsystem == null)
            {
                DisableNotSupported("Collaborative sessions require iOS 13.");
                return;
            }

            subsystem.collaborationRequested = true;
            m_MCSession.Enabled = true;
#else
            DisableNotSupported("Collaborative sessions are an ARKit 3 feature; This platform does not support them.");
#endif
        }

#if UNITY_IOS && !UNITY_EDITOR
    /// <summary>
    /// The multipeersession
    /// </summary>
    MCSession m_MCSession;

    void OnDisable()
    {
        m_MCSession.Enabled = false;
        var subsystem = GetSubsystem();
        if (subsystem != null)
            subsystem.collaborationRequested = false;
    }

    /// <summary>
    /// Returns the ARSession subsystem
    /// </summary>
    ARKitSessionSubsystem GetSubsystem()
    {
        if (m_ARSession == null)
            return null;

        return m_ARSession.subsystem as ARKitSessionSubsystem;
    }

    void Awake()
    {
        m_ARSession = GetComponent<ARSession>();
        m_MCSession = new MCSession(SystemInfo.deviceName, m_ServiceType);
    }

    void Start()
    {
        connectedPeerCount = 0;
    }


    void Update()
    {
        /// <summary>
        /// When network structure changes:
        /// Perform a reset
        /// Enable environment tracking
        /// Disable human body tracking
        /// </summary>
        if (connectedPeerCount != m_MCSession.ConnectedPeerCount)
        {
            connectedPeerCount = m_MCSession.ConnectedPeerCount;
            pointCloudManager.enabled = true;
            scoreCtrl.ResetScores();
            SendScore(scoreCtrl.score);
            humanBodyTracker.enabled = false;
        }

        var subsystem = GetSubsystem();
        if (subsystem == null)
            return;
        // Check if collaboration data is available
        // Collaboration data contains anchor information
        while (subsystem.collaborationDataCount > 0)
        {
            using (var collaborationData = subsystem.DequeueCollaborationData())
            {
                CollaborationNetworkingIndicator.NotifyHasCollaborationData();

                if (m_MCSession.ConnectedPeerCount == 0)
                    continue;

                using (var serializedData = collaborationData.ToSerialized())
                using (var data = NSData.CreateWithBytesNoCopy(serializedData.bytes))
                {
                    // Send collaboration data
                    m_MCSession.SendToAllPeers(data, collaborationData.priority == ARCollaborationDataPriority.Critical
                        ? MCSessionSendDataMode.Reliable
                        : MCSessionSendDataMode.Unreliable);

                    CollaborationNetworkingIndicator.NotifyOutgoingDataSent();

                    // Only log 'critical' data as 'optional' data tends to come every frame
                }
            }
        }

        // Check for incoming data
        while (m_MCSession.ReceivedDataQueueSize > 0)
        {
            CollaborationNetworkingIndicator.NotifyIncomingDataReceived();
            using (var data = m_MCSession.DequeueReceivedData())
            using (var collaborationData = new ARCollaborationData(data.Bytes))
            {
                if (collaborationData.valid) // If received data is collaboration data
                {
                    // Use the collaboration data to update subsystem
                    subsystem.UpdateWithCollaborationData(collaborationData);
                }
                else
                {
                    ReceivedMessage(data);
                }
            }
        }

        if ((m_MCSession.ConnectedPeerCount != 0) && (scoreCtrl.scoreOrder == 0)) // This code block is performed by the main device
        {
            // Only one anchor is created
            if (anchorCtrl.m_Anchors.Count == 0)
            {
                anchorCtrl.CreateAnchor();
            }
            // Environment and body tracking are not possible at the same time
            if(pointCloudManager.enabled == false)
            {
                if(humanBodyTracker.enabled == false)
                {
                    humanBodyTracker.enabled = true;
                    Logger.Log("Human body tracker enabled");
                }
                // Get positions, use them to update the arm model, then send the joint positions relative to the anchor
                positions = humanBodyTracker.GetPositions();
                muscleCtrl.UpdateArm(positions[0], positions[1], positions[2]);           
                SendTrackingData();
            }
        }
        if(scoreCtrl.scoreOrder != 0) // This code block is performed by the peers
        {
            if(humanBodyTracker.enabled == true)
            {
                Logger.Log("Human Body Tracker disabled");
                humanBodyTracker.enabled = false;
            }
            if(pointCloudManager.enabled == true && anchorCtrl.m_Anchors.Count > 0)
            {
                pointCloudManager.enabled = false;
                Logger.Log("Disabled point cloud manager");
                SendReset();
            }
        }
    }
    /// <summary>
    /// Sends joint positions relative to the anchor
    /// </summary>
    void SendTrackingData()
    {
        Debug.Log("Sending Tracking Data");
        SendPositions(RelativePositions(positions));
        Debug.Log("Positions have been sent");
    }
    /// <summary>
    /// Calculates joint positions relative to the anchor
    /// </summary>
    Vector3[] RelativePositions(Vector3[] positions)
    {
        Vector3[] relativePositions = positions;
        ARAnchor anchor = anchorCtrl.m_Anchors[0];
        Vector3 anchorPosition = anchor.transform.position;
        for(int i = 0; i < 3; i++)
        {
            relativePositions[i] -= anchorPosition;
        }
        return relativePositions;
    }
    /// <summary>
    /// Sends a message containing this device's score
    /// </summary>
    void SendScore(double score)
    {
        Message message = new Message();
        message.Type = MessageType.Score;
        message.Score = score;
        SendMessage(message);
    }
    /// <summary>
    /// Sends a message that triggers a reset
    /// </summary>
    void SendReset()
    {
        Message message = new Message();
        message.Type = MessageType.Reset;
        SendMessage(message);
    }
    /// <summary>
    /// Converts a message object to json, then to NSData which is sent to the devices in the multipeer session
    /// </summary>
    void SendMessage(Message message)
    {
        string jsonString = JsonUtility.ToJson(message);
        NativeArray<byte> bytes = new NativeArray<byte>(System.Text.Encoding.UTF8.GetBytes(jsonString), Allocator.Temp);
        NativeSlice<byte> bytesSlice = new NativeSlice<byte>(bytes);
        using (var data = NSData.CreateWithBytesNoCopy(bytesSlice))
        {
            m_MCSession.SendToAllPeers(data, MCSessionSendDataMode.Reliable);
            CollaborationNetworkingIndicator.NotifyOutgoingDataSent();
        }
    }
    /// <summary>
    /// Sends collaboration data
    /// </summary>
    void SendCollaborationData(ARCollaborationData collaborationData)
    {
        CollaborationNetworkingIndicator.NotifyHasCollaborationData();

        if (m_MCSession.ConnectedPeerCount != 0)
        {
            using (var serializedData = collaborationData.ToSerialized())
            using (var data = NSData.CreateWithBytesNoCopy(serializedData.bytes))
            {
                m_MCSession.SendToAllPeers(data, collaborationData.priority == ARCollaborationDataPriority.Critical
                    ? MCSessionSendDataMode.Reliable
                    : MCSessionSendDataMode.Unreliable);

                CollaborationNetworkingIndicator.NotifyOutgoingDataSent();
            }      

        }
    }
    /// <summary>
    /// Performs a specific action for each message type
    /// </summary>
    void ReceivedMessage(NSData data)
    { 
        // Message object is reconstructed from received NSData
        var slice = data.Bytes;
        var bytes = new byte[slice.Length];
        slice.CopyTo(bytes);
        string receivedMessageJson = System.Text.Encoding.UTF8.GetString(bytes);
        Message receivedMessage = JsonUtility.FromJson<Message>(receivedMessageJson);

        if (receivedMessage.Type == MessageType.Score)
        {
            // Add received score
            Logger.Log("Received a Score Message.");
            scoreCtrl.AddScore(receivedMessage.Score);
            scoreCtrl.DetermineScoreOrder();
        }
        if (receivedMessage.Type == MessageType.Reset)
        {
            // Perform a reset
            Logger.Log("Received Reset Message");
            pointCloudManager.enabled = false;
        }
        if (receivedMessage.Type == MessageType.TrackingData)
        {
            // Use received positions to calculate joint positions then update the arm model with them
            Vector3[] positions = receivedMessage.Positions;
            if (anchorCtrl.m_Anchors.Count != 0)
            {
                ARAnchor anchor = anchorCtrl.m_Anchors[0];
                Vector3 anchorPosition = anchor.transform.position;
                muscleCtrl.UpdateArm(positions[0] + anchorPosition, positions[1] + anchorPosition, positions[2] + anchorPosition);
            }
        }
    }
 void OnDestroy()
    {
        m_MCSession.Dispose();
    }
#endif
    }

}
